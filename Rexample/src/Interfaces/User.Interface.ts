export interface IUser
{
    Id: number;
    FullName: string;
    Email: string;
    Password: string;
    Rol: number;
}